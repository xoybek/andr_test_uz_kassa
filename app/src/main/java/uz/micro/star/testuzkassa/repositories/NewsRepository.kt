package uz.micro.star.testuzkassa.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import uz.micro.star.testuzkassa.network.models.news.Article
import uz.micro.star.testuzkassa.network.services.NewsService
import uz.micro.star.testuzkassa.room.NewsDao
import javax.inject.Inject

class NewsRepository @Inject constructor(
    private var service: NewsService,
    private var gson: Gson,
    private var newsDao: NewsDao
) {
    var exception = MutableLiveData<Int>()
    val news = MutableLiveData<List<Article>>()
    val news2 = MutableLiveData<List<Article>>()

    //    fun getAllNewNews(){
//        CoroutineScope(Dispatchers.IO).launch {
//            try {
//                val response = service.getAllNews()
//                when {
//                    response.code() == 200 -> {
//                        withContext(Dispatchers.Main) {
//                            if (response.body() != null) {
//                                news.value = response.body()
//                            }else{
//                                exception.value=1
//                            }
//                        }
//                    }
//                    else -> {
//                        withContext(Dispatchers.Main) {
//                            exception.value=2
//                        }
//                    }
//                }
//            }catch (e: Throwable) {
//                withContext(Dispatchers.Main) {
//                    exception.value=3
//                }
//                Log.d("TTTT", "getAllNewNews->Ooops: Something else went wrong: $e")
//            }
//        }
//    }
    fun getAllNewNews() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = service.getAllNews()
                when {
                    response.code() == 200 -> {
                        withContext(Dispatchers.Main) {
                            if (response.body() != null) {
                                news.value = response.body()!!.articles
                                newsDao.deleteAllNews(1)
                                for (i in response.body()!!.articles.indices) {
                                    val news = response.body()!!.articles[i]
                                    news.type = 1
                                    newsDao.saveNews(news)
                                }
                            } else {
                                exception.value = 1
                            }
                        }
                    }
                    else -> {
                        withContext(Dispatchers.Main) {
                            exception.value = 2
                        }
                    }
                }
            } catch (e: Throwable) {
                withContext(Dispatchers.Main) {
                    news.value = newsDao.getNewsByType(1)
                    exception.value = 3
                }
                Log.d("TTTT", "getAllNewNews->Ooops: Something else went wrong: $e")
            }
        }
    }

    fun getAllNewNews2() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = service.getAllSearchedNews()
                when {
                    response.code() == 200 -> {
                        withContext(Dispatchers.Main) {
                            if (response.body() != null) {
                                news2.value = response.body()!!.articles
                                newsDao.deleteAllNews(2)
                                for (i in response.body()!!.articles.indices) {
                                    val news = response.body()!!.articles[i]
                                    news.type = 2
                                    newsDao.saveNews(news)
                                }
                            } else {
                                exception.value = 1
                            }
                        }
                    }
                    else -> {
                        withContext(Dispatchers.Main) {
                            exception.value = 2
                        }
                    }
                }
            } catch (e: Throwable) {
                withContext(Dispatchers.Main) {
                    news2.value = newsDao.getNewsByType(1)
                    exception.value = 3
                }
                Log.d("TTTT", "getAllNewNews2->Ooops: Something else went wrong: $e")
            }
        }
    }

}