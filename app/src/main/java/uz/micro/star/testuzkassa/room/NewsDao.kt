package uz.micro.star.testuzkassa.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import uz.micro.star.testuzkassa.network.models.news.Article

@Dao
interface NewsDao {

    ////////////////////skins//////////////////////////////////////////////////
    @Query("SELECT * FROM article where type=:type")
    suspend fun getNewsByType(type: Int): List<Article>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveNews(article: Article)

    @Query("DELETE FROM Article where type=:type")
    suspend fun deleteAllNews(type: Int)
    //////////////////////////////////////////////////////////////////////
}