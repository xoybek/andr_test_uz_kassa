package uz.micro.star.testuzkassa.fragments

import android.content.Context
import android.content.Intent
import com.bumptech.glide.Glide
import com.google.gson.Gson
import uz.micro.star.testuzkassa.R
import uz.micro.star.testuzkassa.databinding.FragmentNewsContentBinding
import uz.micro.star.testuzkassa.network.models.news.Article
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class NewsContentFragment :
    BaseFragment<FragmentNewsContentBinding>(FragmentNewsContentBinding::inflate) {

    @Inject
    lateinit var gson: Gson

    override fun onViewCreate() {
        val newsArg = requireArguments().getString("NEWS")
        if (newsArg != null) {
            val news = gson.fromJson(newsArg, Article::class.java)
            binding.apply {
                val dateFormat = SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault())
                dateFormat.format(news.publishedAt!!)
                name.text = news.source!!.name
                title.text = news.title
                time.text = dateFormat.format(news.publishedAt!!)
                Glide.with(binding.root.context)
                    .load(news.urlToImage)
                    .into(binding.image)
                content.text = news.content
                share.setOnClickListener {
                    shareText(requireContext(), news)
                }
            }
        }
    }

    private fun shareText(context: Context, article: Article) {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        val shareBody = """
         ${article.source!!.name}
         
         ${article.title}
          
         ${article.content}
          
         ${article.urlToImage}
         """.trimIndent()
//        sharingIntent.putExtra(
//            Intent.EXTRA_SUBJECT,
//            "https://play.google.com/store/apps/details?id=" + context.packageName
//        )
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        context.startActivity(
            Intent.createChooser(
                sharingIntent,
                "News"
            )
        )
    }
}