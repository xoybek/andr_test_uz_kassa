package uz.micro.star.testuzkassa.fragments

import android.util.DisplayMetrics
import android.view.View
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import uz.micro.star.testuzkassa.R
import uz.micro.star.testuzkassa.adapters.NewsAdapter2
import uz.micro.star.testuzkassa.adapters.NewsAdapter3
import uz.micro.star.testuzkassa.databinding.FragmentNews2Binding
import uz.micro.star.testuzkassa.utils.decorations.NewsMarginItem2Decoration
import uz.micro.star.testuzkassa.utils.decorations.NewsMarginItemDecoration
import uz.micro.star.testuzkassa.utils.getNavOptions
import javax.inject.Inject


class News2Fragment : BaseFragment<FragmentNews2Binding>(FragmentNews2Binding::inflate) {
    lateinit var news2Adapter: NewsAdapter2
    lateinit var news3Adapter: NewsAdapter3

    @Inject
    lateinit var gson: Gson

    override fun onViewCreate() {
        news2Adapter = NewsAdapter2()
        val displaymetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displaymetrics)
        val width = displaymetrics.widthPixels
        news3Adapter = NewsAdapter3(width)
        binding.apply {
            news1.adapter = news2Adapter
            news2Adapter.setItemClickListener {
                val bundle = bundleOf("NEWS" to gson.toJson(it))
                navController.navigate(
                    R.id.action_news2Fragment_to_newsContentFragment,
                    bundle, getNavOptions()
                )
            }
            news1.offscreenPageLimit = 1
            val pageTranslation = resources.getDimension(R.dimen.margin_padding_56)
            val pageTransformer = ViewPager2.PageTransformer { page: View, position: Float ->
                page.translationX = -pageTranslation * position
            }
            news1.setPageTransformer(pageTransformer)
            news1.addItemDecoration(
                NewsMarginItemDecoration(
                    resources.getDimension(R.dimen.margin_padding_32).toInt(),
                )
            )
            ////////////////////////////////////////////////////////////
            news2.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            news2.adapter = news3Adapter
            news3Adapter.setItemClickListener {
                val bundle = bundleOf("NEWS" to gson.toJson(it))
                navController.navigate(
                    R.id.action_news2Fragment_to_newsContentFragment,
                    bundle, getNavOptions()
                )
            }
            news2.addItemDecoration(
                NewsMarginItem2Decoration(
                    requireContext().resources.getDimension(R.dimen.margin_padding_16).toInt()
                )
            )
        }
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.newsRepository.getAllNewNews()
            viewModel.newsRepository.getAllNewNews2()
        }
        getAllNewNews()
    }

    fun getAllNewNews() {
        viewModel.newsRepository.exception.observe(this, {
            binding.shimmer1.visibility = View.GONE
            binding.shimmer1.stopShimmer()
            binding.shimmer2.visibility = View.GONE
            binding.shimmer2.stopShimmer()
            when (it) {
                1 -> {
                    Snackbar.make(binding.news1, "Has not news", Snackbar.LENGTH_LONG).show()
                }
                2 -> {
                    Snackbar.make(binding.news1, "Something went wrong", Snackbar.LENGTH_LONG)
                        .show()
                }
//                3 -> {
//                    Snackbar.make(binding.news1, "Server is not working", Snackbar.LENGTH_LONG)
//                        .show()
//                }
            }
        })
        viewModel.newsRepository.news.observe(this, {
            binding.shimmer1.visibility = View.GONE
            binding.shimmer1.stopShimmer()
            if (it != null) {
                binding.swipeRefresh.isRefreshing = false
                news2Adapter.setNews(it)
//                binding.news1.visibility = View.VISIBLE
            }
        })
        if (viewModel.newsRepository.news.value == null) viewModel.newsRepository.getAllNewNews()
        /////////////////////////
        viewModel.newsRepository.news2.observe(this, {
            binding.shimmer2.visibility = View.GONE
            binding.shimmer2.stopShimmer()
            if (it != null) {
                binding.swipeRefresh.isRefreshing = false
                news3Adapter.setNews(it)
//                binding.news1.visibility = View.VISIBLE
            }
        })
        if (viewModel.newsRepository.news2.value == null) viewModel.newsRepository.getAllNewNews2()
    }
}