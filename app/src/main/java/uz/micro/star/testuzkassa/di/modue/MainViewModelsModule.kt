package uz.micro.star.testuzkassa.di.modue

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import uz.micro.star.testuzkassa.viewmodels.MainActivityViewModel
import uz.micro.star.testuzkassa.di.scopes.ViewModelKey

@Module
abstract class MainViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun homeViewModel(mainActivityViewModel: MainActivityViewModel): ViewModel
}