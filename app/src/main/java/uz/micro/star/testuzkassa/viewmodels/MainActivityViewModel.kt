package uz.micro.star.testuzkassa.viewmodels

import androidx.lifecycle.ViewModel
import uz.micro.star.testuzkassa.repositories.NewsRepository

import javax.inject.Inject

/**
 * Created by Microstar on 27.09.2020.
 */
class  MainActivityViewModel @Inject constructor(
    var newsRepository: NewsRepository
) : ViewModel() {
    var cardNumber: String = ""
}
