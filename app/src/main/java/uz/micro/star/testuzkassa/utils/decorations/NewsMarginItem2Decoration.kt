package uz.micro.star.testuzkassa.utils.decorations

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Microstar on 02.06.2020.
 */
class NewsMarginItem2Decoration(private val spaceHeight: Int) :
    RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: RecyclerView, state: RecyclerView.State
    ) {
        with(outRect) {
            left = if(parent.getChildLayoutPosition(view)==0){
                spaceHeight
            }else{
                0
            }
            right = spaceHeight
//            left = spaceHeight
            top = 0
            bottom = 0
        }
    }
}