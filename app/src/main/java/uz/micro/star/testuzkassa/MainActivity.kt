package uz.micro.star.testuzkassa

import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar

import dagger.android.support.DaggerAppCompatActivity
import uz.micro.star.testuzkassa.databinding.ActivityMainBinding
import uz.micro.star.testuzkassa.dialogs.NoConnectionDialog
import uz.micro.star.testuzkassa.utils.NetworkConnectionListener
import uz.micro.star.testuzkassa.utils.statusBarColor
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    lateinit var navController: NavController

    @Inject
    lateinit var networkListener: NetworkConnectionListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        statusBarColor(
            ResourcesCompat.getColor(
                resources, R.color.white, theme
            ), ResourcesCompat.getColor(
                resources, R.color.white, theme
            ), true
        )
        navController = findNavController(R.id.main_nav_fragment)

//        val dialog = NoConnectionDialog(this)
        val snackbar = Snackbar.make(binding.root, "Offline mode", Snackbar.LENGTH_LONG)
        if (networkListener.isNetworkAvailable()){
            snackbar.dismiss()
        } else {
            snackbar.show()
        }
        networkListener.observe(this, {
            if (it) {
//                dialog.dismiss()
                snackbar.dismiss()
            } else {
//                dialog.show()
                snackbar.show()
            }
        })
    }
}