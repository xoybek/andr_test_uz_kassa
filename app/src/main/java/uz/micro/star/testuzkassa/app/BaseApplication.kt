package uz.micro.star.testuzkassa.app

import dagger.android.support.DaggerApplication
import uz.micro.star.testuzkassa.di.component.DaggerAppComponent


/**
 * Created by Microstar on 19.08.2021
 */
class BaseApplication : DaggerApplication() {
    override fun applicationInjector() = DaggerAppComponent.builder().application(this).build()
}