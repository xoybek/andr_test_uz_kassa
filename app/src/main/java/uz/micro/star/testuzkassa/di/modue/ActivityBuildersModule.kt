package uz.micro.star.testuzkassa.di.modue

import dagger.Module
import dagger.android.ContributesAndroidInjector
import uz.micro.star.testuzkassa.MainActivity

/**
 * Created by Microstar on 19.08.2021
 */

@Module
abstract class ActivityBuildersModule {
    @ContributesAndroidInjector(modules = [MainFragmentBuildersModule::class, MainViewModelsModule::class])
    abstract fun contributeMainActivity(): MainActivity
}