package uz.micro.star.testuzkassa.network.services

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import uz.micro.star.testuzkassa.network.models.news.NewsResponse
import uz.micro.star.testuzkassa.utils.Constants


interface NewsService {

    @GET("top-headlines")
    suspend fun getAllNews(
        @Query("apiKey") closeReason: String = Constants.TOKEN,
        @Query("q") country: String = "usa",
    ): Response<NewsResponse>

    @GET("top-headlines")
    suspend fun getAllSearchedNews(
        @Query("apiKey") closeReason: String = Constants.TOKEN,
        @Query("q") country: String = "bitcoin",
    ): Response<NewsResponse>


}