package uz.micro.star.testuzkassa.network.models.news


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "source")
data class Source(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    var id: String,
    @SerializedName("name")
    var name: String
)