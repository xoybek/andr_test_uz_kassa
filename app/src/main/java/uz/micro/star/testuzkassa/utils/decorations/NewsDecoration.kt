package uz.micro.star.testuzkassa.utils.decorations

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Microstar on 19.08.2021
 */
class NewsDecoration(private val spaceHeight: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: RecyclerView, state: RecyclerView.State
    ) {
        with(outRect) {
            top = if (parent.getChildAdapterPosition(view) == 0) {
                spaceHeight
            } else {
                0
            }
            left = spaceHeight
            right = spaceHeight
            bottom = spaceHeight
        }
    }
}