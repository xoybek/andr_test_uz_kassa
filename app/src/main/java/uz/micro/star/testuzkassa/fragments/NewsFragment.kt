package uz.micro.star.testuzkassa.fragments

import android.view.View
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import uz.micro.star.testuzkassa.R
import uz.micro.star.testuzkassa.adapters.NewsAdapter
import uz.micro.star.testuzkassa.databinding.FragmentNewsBinding
import uz.micro.star.testuzkassa.utils.decorations.NewsDecoration
import uz.micro.star.testuzkassa.utils.getNavOptions
import javax.inject.Inject


class NewsFragment : BaseFragment<FragmentNewsBinding>(FragmentNewsBinding::inflate) {
    lateinit var newsAdapter: NewsAdapter

    @Inject
    lateinit var gson: Gson

    override fun onViewCreate() {

        newsAdapter = NewsAdapter()
        newsAdapter.setItemClickListener {
            val bundle = bundleOf("NEWS" to gson.toJson(it))
            navController.navigate(
                R.id.action_newsFragment_to_newsContentFragment,
                bundle, getNavOptions()
            )
        }
        binding.apply {
            newsList.layoutManager = LinearLayoutManager(requireContext())
            newsList.adapter = newsAdapter
            newsList.addItemDecoration(
                NewsDecoration(
                    requireContext().resources.getDimension(R.dimen.margin_padding_16).toInt()
                )
            )
        }
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.newsRepository.getAllNewNews()
        }
        getAllNewNews()
    }

    fun getAllNewNews() {
        viewModel.newsRepository.exception.observe(this, {
            binding.shimmer.visibility = View.GONE
            binding.shimmer.stopShimmer()
            when (it) {
                1 -> {
                    Snackbar.make(binding.newsList, "Has not news", Snackbar.LENGTH_LONG).show()
                }
                2 -> {
                    Snackbar.make(binding.newsList, "Something went wrong", Snackbar.LENGTH_LONG)
                        .show()
                }
                3 -> {
                    Snackbar.make(binding.newsList, "Server is not working", Snackbar.LENGTH_LONG)
                        .show()
                }
            }
        })
        viewModel.newsRepository.news.observe(this, {
            binding.shimmer.visibility = View.GONE
            binding.shimmer.stopShimmer()
            if (it != null) {
                binding.swipeRefresh.isRefreshing = false
                newsAdapter.setNews(it)
                binding.newsList.visibility = View.VISIBLE
            }
        })
        if (viewModel.newsRepository.news.value == null) viewModel.newsRepository.getAllNewNews()
    }
}