package uz.micro.star.testuzkassa.di.modue

import dagger.Module
import dagger.android.ContributesAndroidInjector
import uz.micro.star.testuzkassa.fragments.News2Fragment
import uz.micro.star.testuzkassa.fragments.NewsContentFragment
import uz.micro.star.testuzkassa.fragments.NewsFragment

/**
 * Created by Microstar on 19.08.2021
 */
@Module
abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun newsFragment(): NewsFragment

    @ContributesAndroidInjector
    abstract fun newsContentFragment(): NewsContentFragment

    @ContributesAndroidInjector
    abstract fun news2Fragment(): News2Fragment


}