package uz.micro.star.testuzkassa.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import uz.micro.star.testuzkassa.databinding.ItemNewsBinding
import uz.micro.star.testuzkassa.network.models.news.Article
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Microstar on 019 19.08.21.
 */
class NewsAdapter : RecyclerView.Adapter<NewsAdapter.MovieCardViewHolder>() {
    private var itemClickListener: ((data: Article) -> Unit)? = null
    fun setItemClickListener(f: (data: Article) -> Unit) {
        itemClickListener = f
    }

    private var data = mutableListOf<Article>()

    fun setNews(data: List<Article>) {
        this.data.clear()
        this.data.addAll(data)
        this.notifyDataSetChanged()
    }

    inner class MovieCardViewHolder(private val binding: ItemNewsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(data: Article) {
            binding.apply {
//                val calendar=DateFormat.getInstance().format(data.publishedAt)
                val dateFormat = SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault())
                dateFormat.format(data.publishedAt!!)
                name.text = data.source!!.name
                title.text = data.title
                time.text = dateFormat.format(data.publishedAt!!)
                Glide.with(binding.root.context)
                    .load(data.urlToImage)
                    .into(binding.image)
                itemView.setOnClickListener {
                    itemClickListener?.invoke(data)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MovieCardViewHolder(
        ItemNewsBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    )

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: MovieCardViewHolder, position: Int) =
        holder.bindData(data[position])
}