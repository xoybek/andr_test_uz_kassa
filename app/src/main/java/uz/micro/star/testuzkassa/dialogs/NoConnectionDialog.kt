package uz.micro.star.testuzkassa.dialogs

import android.content.Context
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import uz.micro.star.testuzkassa.databinding.DialogNoConnectionBinding

class NoConnectionDialog(context: Context) : AlertDialog(context) {
    val binding = DialogNoConnectionBinding.inflate(LayoutInflater.from(context))
    init {
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        setCancelable(false)
        setView(binding.root)
    }
}