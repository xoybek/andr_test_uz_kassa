package uz.micro.star.testuzkassa.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.viewbinding.ViewBinding
import dagger.android.support.DaggerFragment
import uz.micro.star.testuzkassa.utils.Inflate
import uz.micro.star.testuzkassa.utils.SharedPreferencesHelper
import uz.micro.star.testuzkassa.utils.dismissKeyboard
import uz.micro.star.testuzkassa.viewmodels.DaggerViewModelFactory
import uz.micro.star.testuzkassa.viewmodels.MainActivityViewModel
import javax.inject.Inject


abstract class BaseFragment<VB : ViewBinding>(
    private val inflate: Inflate<VB>
) : DaggerFragment() {
    private var _binding: VB? = null
    val binding get() = _binding!!

    lateinit var viewModel: MainActivityViewModel
    lateinit var navController: NavController

    @Inject
    lateinit var shared: SharedPreferencesHelper

    @Inject
    lateinit var providerFactory: DaggerViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dismissKeyboard(requireActivity())
        navController = Navigation.findNavController(view)
        viewModel = ViewModelProviders.of(
            requireActivity(),
            providerFactory
        )[MainActivityViewModel::class.java]
        onViewCreate()
    }

    abstract fun onViewCreate()

}
