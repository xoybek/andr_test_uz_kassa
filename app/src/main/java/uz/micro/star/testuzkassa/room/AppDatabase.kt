package uz.micro.star.testuzkassa.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import uz.micro.star.testuzkassa.network.models.news.Article


@Database(
    entities = [
        Article::class,
    ], version = 1
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getCardDao(): NewsDao
}